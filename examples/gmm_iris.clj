(ns gmm-iris
  (:require [clojure.set
             :refer [intersection]]
            [clojure.core.matrix
             :refer [identity-matrix diagonal-matrix inverse]
             :rename {identity-matrix eye
                      diagonal-matrix diag}])
  (:use [embang emit runtime]
        iris-data))

(def shuffled-iris-data (shuffle iris-data))

(def shuffled-iris-partition
  (into {} (map (fn [l]
                  (let [ns (keep-indexed (fn [n r]
                                           (when (= (last r) l)
                                             n))
                                         shuffled-iris-data)]
                    [l (set ns)]))
                [:setosa :versicolor :virginica])))

(defn part [labels]
  (loop [ptn {}
         n 0
         ls labels]
    (if-let [l (first ls)]
      (recur (assoc ptn
               l (conj (or (ptn l) #{}) n))
             (inc n)
             (rest ls))
      ptn)))

(defn best-match [index-set zs]
  (loop [; count of elements in index set for each class label
         c {}
         ; class label of each datapoint
         zs zs
         ; index in index-set
         n 0]
    (if-let [z (first zs)]
      (if (index-set n)
        (recur (assoc c
                 z (inc (or (c z) 0)))
               (rest zs)
               (inc n))
        (recur c
               (rest zs)
               (inc n)))
      ; return index with largest value in c
      (key (apply max-key val c)))))

(with-primitive-procedures [eye diag best-match]
  (defanglican gmm-iris
    ; number of mixture components
    [assume K 3]
    ; prior on mean and covariance
    [assume unit-normal (normal 0 1)]
    [assume precision-prior (gamma 1.0 0.06)]
    ; class means
    [assume means (repeatedly K (lambda ()
                                  (map (lambda (m)
                                         (+ m (sample unit-normal)))
                                       iris-mean)))]
    ; class covariances
    [assume covs (repeatedly K
                  (lambda ()
                    (repeatedly 4
                      (lambda ()
                        (/ (sample precision-prior))))))]

    ; likelihoods
    [assume likes (map (lambda (m c)
                         (mvn m (diag c)))
                       means
                       covs)]
    ; prior on class labels
    [assume class-prior (discrete (repeat K (/ 1.0 K)))]
    ; sample class labels (and do observes)
    [assume zs (reduce (lambda (zs record)
                         (let ((z (sample class-prior)))
                           [observe (nth likes z) (butlast record)]
                           (conj zs z)))
                       '[]
                       shuffled-iris-data)]
    ; get class label that is the best match to the :setosa label in the data
    [assume mu (lambda (k d)
                 (nth (nth means k) d))]
    [assume setosa (best-match (get shuffled-iris-partition :setosa) zs)]
    ; predict mean
    [predict (mu setosa 0)]
    [predict (mu setosa 1)]
    [predict (mu setosa 2)]
    [predict (mu setosa 3)]))

(with-primitive-procedures [eye diag inverse]
  (defanglican gmm-iris-sampled-cov
    ; number of mixture components
    [assume K 3]
    ; prior on covariance
    [assume n 5]
    [assume V '[[ 2.1, -1. , -1.5,  0.5],
                [-1. ,  2.5,  0.6, -1.1],
                [-1.5,  0.6,  2.3, -1.9],
                [ 0.5, -1.1, -1.9,  8.5]]]
    ; prior on mean
    [assume mean-prior (mvn iris-mean (eye 4))]
    ; parameters
    [assume cov (inverse (sample (wishart n V)))]
    [assume means (repeatedly K
                    (lambda ()
                      (sample mean-prior)))]
    ; likelihoods
    [assume likes (map (lambda (m)
                         (mvn m cov))
                        means)]
    ; prior on class labels
    [assume class-prior (discrete (repeat K (/ 1.0 K)))]
    ; observations
    (reduce (lambda (_ record)
              [observe (nth likes (sample class-prior)) (butlast record)]
              nil
              shuffled-iris-data))
    ; predict state means
    [assume mu (lambda (k d)
                 (nth (nth means k) d))]
    ; means of class 0
    [predict (mu 0 0)]
    [predict (mu 0 1)]
    [predict (mu 0 2)]
    [predict (mu 0 3)]))

(with-primitive-procedures [eye]
  (defanglican gmm-iris-nomem
    ; number of mixture components
    [assume K 3]
    ; prior on mean
    [assume unit-normal (normal 0 1)]
    ; class means
    [assume means (repeatedly K
                    (lambda ()
                      (map (lambda (m)
                             (+ m (sample unit-normal)))
                           iris-mean)))]
    ; likelihoods
    [assume likes (map (lambda (m)
                         (mvn m iris-cov))
                        means)]
    ; prior on class labels
    [assume class-prior (discrete (repeat K (/ 1.0 K)))]
    ; observations
    (reduce (lambda (_ record)
              [observe (nth likes (sample class-prior)) (butlast record)])
              nil
              shuffled-iris-data)
    ; predict state means
    [assume mu (lambda (k d)
                 (nth (nth means k) d))]
    ; means of class 0
    [predict (mu 0 0)]
    [predict (mu 0 1)]
    [predict (mu 0 2)]
    [predict (mu 0 3)]))


