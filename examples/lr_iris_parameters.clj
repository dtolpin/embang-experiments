(ns lr-iris-parameters
  (:use [embang runtime emit]
        iris-data))

(defanglican lr-iris-parameters
  [assume dot-product
   (lambda (u v) 
     (if (= (count u) 0) 0 
       (+ (* (first u) (first v))
          (dot-product (rest u) (rest v)))))]
  [assume sigma (sqrt (sample (gamma 1 1)))]
  [assume b (repeatedly
              5 (lambda () (sample (normal 0. sigma))))]
  [assume z (lambda (x) 
              (/ 1. (+ 1. (exp (* -1. (dot-product b x))))))]
  (reduce (lambda (_ record)
            (observe (flip (z (cons 1 (butlast record))))
                     (= (last record) iris-setosa)))
          () iris-data)

  [predict sigma]
  [predict (nth b 0)]
  [predict (nth b 1)]
  [predict (nth b 2)]
  [predict (nth b 3)]
  [predict (nth b 4)])
