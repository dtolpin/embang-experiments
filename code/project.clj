(defproject almh "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [org.clojure/tools.cli "0.3.1"]
                 [org.clojure/data.priority-map "0.0.5"]
                 [org.clojure/data.json "0.2.5"]
                 [com.taoensso/timbre "3.3.1"]
                 [colt "1.2.0"]
                 [org.clojure/math.combinatorics "0.0.8"]
                 [net.mikera/core.matrix "0.32.1"]
                 [net.mikera/vectorz-clj "0.28.0"]]
  :main ^:skip-aot almh.core
  :target-path "target/%s"
  :resource-paths ["../../m/code/src"]
  :profiles {:uberjar {:aot :all}})
