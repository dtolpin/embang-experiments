(ns almh.core
  (:gen-class)
  (:require [clojure.string :as str]
            [clojure.java.io :as io])
  (:use embang.results))


(defn print-row
  "prints a row of values"
  [values]
  (println (str/join "," (map str values))))


(defn choice-seq
  "reads predicts with choice statistics on stdin and outputs
  a sequence of lines with rewards, weights, and counts"
  []
  (letfn
    [(choice-seq* [lines]
       (lazy-seq
         (when-let [[[label value] & lines] (seq lines)]
           (case label
             $choice-rewards 
             (list* (cons "rewards" (map #(nth % 1) value))
                    (cons "weights" (map #(nth % 2) value))
                    (choice-seq* lines))

             $choice-counts
             (list* (cons "counts" (map #(nth % 1) value))
                    (choice-seq* lines))

             (choice-seq* lines)))))]
    (choice-seq* (parsed-line-seq (line-seq (io/reader *in*))))))


  (defn -main
    "extracts choice statistics"
    [& args]
    (doseq [row (choice-seq)]
      (print-row row)))
